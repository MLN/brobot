module.exports = function (bot, channel) {
	// status text by nickname, e.g. { langan: 'joined at 3:30' }
	var seen = {};

	function saw (nick, message, ifUnseen, append) {
		nick = nick.toLowerCase();
		if (ifUnseen && nick in seen) { return; }
		var date = new Date();
		seen[nick] = message + ' at ' + date.toLocaleTimeString() + ' (' + date.toLocaleDateString() + ')';
	}

	bot.addListener('names' + channel, function (nicks) {
		for (var nick in nicks) {
			saw(nick, 'was already here', true);
		}
	});

	bot.addListener('join' + channel, function (nick) {
		saw(nick, 'joined', true);
	});

	bot.addListener('part' + channel, function (nick) {
		saw(nick, 'left', false);
	});

	bot.addListener('message' + channel, function (from, text, message) {
		saw(from, 'said "'+text+'"');
	});

	// FIXME this currently does not work: https://github.com/martynsmith/node-irc/issues/212
	bot.addListener('action' + channel, function(from, to, text, message) {
		saw(from, 'was all "'+text+'"');
	});

	return {
		lookFor: function (nick, looker) {
			if (nick == bot.nick.toLowerCase()) {
				return "I'm here, whether I like it or not.";
			} else if (nick == looker) {
				return 'Long night, eh?'
			} else if (nick in seen) {
				if (looker.toLowerCase() in seen) {
					return nick + ' ' + seen[nick];
				} else { // someone not on the channel
					return "Yeah, I've seen " + nick + ". Who's askin'?";
				}
			} else {
				return "I haven't seen " + nick;
			}
		}
	}
};
