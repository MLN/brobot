module.exports.teachBotHowToLearnUserCommands = function(bot, newCommand, forgetCommand, learnedListCommand) {
	var newCommand 			= newCommand 		 || /!respondTo (.*) with (.*)/,
		forgetCommand 		= forgetCommand 	 || /!forget (.*)/,
		learnedListCommand 	= learnedListCommand || /!userCommands/;

	if (typeof bot.respondTo === 'function' &&
		typeof bot.say 		 === 'function') {

		var thisbot = bot;
		thisbot.learnedTricks = {};

		var learnNewTrick = function (from, target, text, match, message) {
			var trigger = match[1],
				response = match[2];

			//remove !
			while(trigger[0] === '!') trigger = trigger.slice(1);

			if(response) {
				thisbot.respondTo(new RegExp(trigger), function(from, target){ thisbot.say(target, response); });
				thisbot.say(target, 'I learned what to say to "'+trigger+'"" today. Thanks '+from);
			} else {
				thisbot.respondTo(new RegExp(trigger), function(){ /*do nothing*/ });
				thisbot.say(target, 'I forgot about "'+trigger+'"');
			}
			thisbot.learnedTricks[trigger] = response;
		};

 		var speakTrickList = function (from, target, text, match, message) {
 			for (trigger in thisbot.learnedTricks){
 				var response = thisbot.learnedTricks[trigger];
 				thisbot.say(target,trigger+' -> '+response);
 			}
 		};

		thisbot.respondTo(newCommand, learnNewTrick );
		thisbot.respondTo(forgetCommand, learnNewTrick );
		thisbot.respondTo(learnedListCommand, speakTrickList );

	} else {
		console.log("\n hey, sorry dude, I can't work with this guy " + bot.toString() + ", like, at all. \
			         \n I tried to teach it how to listen, but it doesnt understand respondTo() AND say() \n");
	}
};
