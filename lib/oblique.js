var fs = require('fs');

var strategies = null;
fs.readFile('./lib/oblique-strategies.txt', function (e, data) {
	if (e) {
		console.log("Can't load oblique strategies file: " + e);
		return;
	}

	strategies = data.toString().split('\n');
});

exports.random = function () {
	return strategies[Math.floor(Math.random()*strategies.length)];
};

if (require.main === module) {
	console.log(exports.random());
}
