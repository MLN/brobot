#!/usr/bin/env node

var botFactory = require('./SetupBot.js'),
    brobot = botFactory('brobot', '#ritbros'),
    strategies = require('./lib/oblique'),
    watcher = require('./lib/watcher')(brobot, '#ritbros'),
    edjumacate = require('./lib/learner').teachBotHowToLearnUserCommands,
    schedule = require('node-schedule');

edjumacate(brobot);

brobot.respondTo(/^(|good )(hello|hi|\'?evening|\'?morning|yo[^u])/, function (from, respondTo, text, match, message) {
    var date = new Date(), greeting = 'こんにちは、';
    if (date.getHours() < 10) { greeting = 'おはよう、'; }
    if (date.getHours() > 17) { greeting = 'こんばんは、'; }

    this.say(respondTo, greeting + from);
});

brobot.respondTo(/^(|good ?)(\'?later|\'?night)/, function (from, respondTo, text, match, message) {
    var date = new Date(), greeting = 'さようなら、';
    if (date.getHours() < 4 || date.getHours() > 21) { greeting = 'お疲れ様、'; }

    this.say(respondTo, greeting + from);
});

brobot.respondTo(/oblique/, function (from, respondTo, text, match, message) {
    brobot.say(respondTo, '“' + strategies.random() + '”');
});

brobot.respondTo(/^!seen ([-\w]+)/, function (from, respondTo, text, match, message) {
    var nick = match[1].toLowerCase();
    this.say(respondTo, watcher.lookFor(nick, from));
});
brobot.respondTo(/^tellAndrew (.*)/, function (from, respondTo, text, match, message) {
    this.say('andrew', match[0]);
});

schedule.scheduleJob({hour: 16, minute: 20}, function () {
    brobot.say('#apdeng', 'duuuuuuuuuude... have you ever really _looked_ at your hand?');
});
