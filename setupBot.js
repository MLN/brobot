var irc = require('irc');

var server = 'bonjarber.com';

module.exports = function (name, channel) {
	var config = { autoConnect: false };
	if (channel) { config.channels = [channel]; }

	var bot = new irc.Client(server, name, config);

	var triggers = {};

	bot.addListener('message', function(from, to, text, message) {
		// this == bot
		if (from == this.nick) { return; }
		console.log(from + ' said "' + text + '" to ' + to);

		var respondTo, pattern, regex, match;
		respondTo = (to == this.nick ? from : to);

		for (pattern in triggers) {
			regex = new RegExp(pattern);
			match = text.match(regex);
			match && triggers[pattern].call(this, from, respondTo, text, match, message);
		}
	});

	bot.respondTo = function (regex, reaction) {
		if (triggers[regex.source]) {
			console.warn('replacing previous response for ' + regex);
		}

		if (typeof reaction == 'string') {
			var message = reaction;
			reaction = function (from, respondTo) { this.say(respondTo, message); }
		}

		triggers[regex.source] = reaction;
		console.log(name + ' registered response to ' + regex.source);
	};

	bot.respondTo(new RegExp('^'+ name, 'i'), "That's me!");

	bot.respondTo(/^!help/, function (from, respondTo, text, match, message) {
		var trigger_names = [];
		for (var name in triggers) { trigger_names.push('/' + name + '/'); }
		var usage = [
			'I know how to respond to these regex triggers:',
			trigger_names.sort().join(', '),
			'(I can also respond on a private channel, if you wish to keep our conversation private.)'
		].join('\n');
		this.say(respondTo, usage);
	});

	bot.connect();

	return bot;
};

// the returned bot(s) listen for all sorts of events,
// e.g. bot.addListener(<event>, function () { ... });
// see https://node-irc.readthedocs.org/en/latest/API.html
//   registered
//   action
//   motd
//   names
//   names#apdeng
//   topic
//   join
//   join#apdeng
//   part
//   part#apdeng
//   quit
//   kick
//   kick#apdeng
//   kill
//   message
//   message#
//   message#apdeng
//   notice
//   ping
//   pm
//   nick
//   invite
//   whois
//   channellist
//   error
